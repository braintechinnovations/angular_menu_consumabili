import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConsumabileDetailComponent } from './consumabile-detail/consumabile-detail.component';
import { ConsumabileInsertComponent } from './consumabile-insert/consumabile-insert.component';
import { ConsumabileListComponent } from './consumabile-list/consumabile-list.component';
import { ConsumabileUpdateComponent } from './consumabile-update/consumabile-update.component';
import { ProveInterneComponent } from './prove-interne/prove-interne.component';

const routes: Routes = [
  {path: "", redirectTo: "consumabile/list", pathMatch: "full"},
  {path: "consumabile/insert", component: ConsumabileInsertComponent},
  {path: "consumabile/detail/:codice/:nome", component: ConsumabileDetailComponent},
  {path: "consumabile/detail/:codice", component: ConsumabileDetailComponent},
  {path: "consumabile/list", component: ConsumabileListComponent},
  {path: "consumabile/update", component: ConsumabileUpdateComponent},
  {path: "prove-interne", component: ProveInterneComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
