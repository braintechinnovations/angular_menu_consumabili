export class Consumabile {

    id : number | undefined;
    codice : string | undefined;
	nome : string | undefined;
	descrizione : string | undefined;
	tipologia : string | undefined;

}
