import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ConsumabileListComponent } from './consumabile-list/consumabile-list.component';
import { ConsumabileInsertComponent } from './consumabile-insert/consumabile-insert.component';
import { ConsumabileDetailComponent } from './consumabile-detail/consumabile-detail.component';
import { ConsumabileUpdateComponent } from './consumabile-update/consumabile-update.component';
import { ProveInterneComponent } from './prove-interne/prove-interne.component';
import { FormsModule } from '@angular/forms';
import { HttpClient, HttpClientModule, HttpHandler } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    ConsumabileListComponent,
    ConsumabileInsertComponent,
    ConsumabileDetailComponent,
    ConsumabileUpdateComponent,
    ProveInterneComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,        //Necessario per il Data Binding con l'HTML
    HttpClientModule    //Importa tutto il pacchetto HTTP evitando di integrare ogni singolo provider all'interno di Providers
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
