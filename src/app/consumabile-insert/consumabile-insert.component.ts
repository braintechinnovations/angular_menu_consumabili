import { TmplAstTemplate } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Consumabile } from '../consumabile';
import { RestService } from '../rest.service';

@Component({
  selector: 'app-consumabile-insert',
  templateUrl: './consumabile-insert.component.html',
  styleUrls: ['./consumabile-insert.component.css']
})
export class ConsumabileInsertComponent implements OnInit {

  inputCodice : string;
  inputNome : string;
  inputDescrizione : string;
  inputTipologia : string;

  constructor(private rest : RestService, private router : Router) { 

  }

  ngOnInit(): void {
  }

  inserisciConsumabile(){

    let temp = new Consumabile();
    temp.codice = this.inputCodice;
    temp.nome = this.inputNome;
    temp.descrizione = this.inputDescrizione;
    temp.tipologia = this.inputTipologia;

    this.rest.addConsumabile(temp)
        .subscribe(
          function(risultato){              //Equivalente Success di AJAX
            alert("Tutto è andato bene!");
          },
          function(errore){                 //Equivalente Error di AJAX
            console.log(errore);
          }
        );

    // this.rest.addConsumabile(temp)
    //     .subscribe(
    //       (risultato) => {              //Equivalente Success di AJAX
    //         alert("Tutto è andato bene!");
    //       },
    //       (errore) => {                 //Equivalente Error di AJAX
    //         console.log(errore);
    //       }
    //     );
  }

}
