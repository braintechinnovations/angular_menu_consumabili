import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsumabileInsertComponent } from './consumabile-insert.component';

describe('ConsumabileInsertComponent', () => {
  let component: ConsumabileInsertComponent;
  let fixture: ComponentFixture<ConsumabileInsertComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsumabileInsertComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsumabileInsertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
