import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsumabileUpdateComponent } from './consumabile-update.component';

describe('ConsumabileUpdateComponent', () => {
  let component: ConsumabileUpdateComponent;
  let fixture: ComponentFixture<ConsumabileUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsumabileUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsumabileUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
