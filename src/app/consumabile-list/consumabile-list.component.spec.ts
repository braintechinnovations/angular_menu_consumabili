import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsumabileListComponent } from './consumabile-list.component';

describe('ConsumabileListComponent', () => {
  let component: ConsumabileListComponent;
  let fixture: ComponentFixture<ConsumabileListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsumabileListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsumabileListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
