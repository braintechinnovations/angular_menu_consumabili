import { prepareSyntheticPropertyName } from '@angular/compiler/src/render3/util';
import { Component, OnInit } from '@angular/core';
import { RestService } from '../rest.service';

@Component({
  selector: 'app-consumabile-list',
  templateUrl: './consumabile-list.component.html',
  styleUrls: ['./consumabile-list.component.css']
})
export class ConsumabileListComponent implements OnInit {

  elenco : any = [];

  constructor(private rest : RestService) { 
    
  }

  ngOnInit(): void { 

    this.rest.getAllConsumabili().subscribe(
      (responso) => {
        this.elenco = responso;
        console.log(responso);
      }, (errore) => {
        console.log(errore);
      }
    );

  }

}
