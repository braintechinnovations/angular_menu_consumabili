import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { RouterLink } from '@angular/router';
import { Consumabile } from './consumabile';

const endpoint = "http://localhost:8080/consumabili/";

@Injectable({
  providedIn: 'root'
})
export class RestService {

  // private http : HttpClient | undefined;
  // constructor(){
  //   this.http = new HttpClient();
  // }

  constructor(private http: HttpClient) { 

  }

  //TODO: Post di inserimento
  addConsumabile(objConsumabile){
    //url: http://localhost:8081/consumabili/insert

    let headers_custom = new HttpHeaders();
    headers_custom = headers_custom.set('Content-Type', 'application/json');

    return this.http.post(endpoint + "insert", JSON.stringify(objConsumabile), {headers: headers_custom});
  }

  //TODO: Get Lista oggetti
  getAllConsumabili(){
    return this.http.get(endpoint + "list");
  }

  //TODO: Get dettaglio oggetto
  getDettaglioOggetto(varCodice : String){
    console.log("---------> " + endpoint + varCodice);
    return this.http.get<Consumabile>(endpoint + varCodice);
  }
  //TODO: Delete eliminazione oggetto
  deleteOggetto(varCodice : String){
    console.log("---------> " + endpoint + "delete/" + varCodice);
    return this.http.delete<Boolean>(endpoint + "delete/" + varCodice);
  }
  //TODO: Put modifica oggetto
  updateOggetto(objConsumabile : Consumabile){
    let headers_custom = new HttpHeaders();
    headers_custom = headers_custom.set('Content-Type', 'application/json');

    return this.http.put<Consumabile>(endpoint + "update/" + objConsumabile.codice, JSON.stringify(objConsumabile), {headers: headers_custom});
  }

}
