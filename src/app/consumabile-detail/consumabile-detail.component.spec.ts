import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsumabileDetailComponent } from './consumabile-detail.component';

describe('ConsumabileDetailComponent', () => {
  let component: ConsumabileDetailComponent;
  let fixture: ComponentFixture<ConsumabileDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsumabileDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsumabileDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
