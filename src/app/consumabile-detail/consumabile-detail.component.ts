import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Consumabile } from '../consumabile';
import { RestService } from '../rest.service';

@Component({
  selector: 'app-consumabile-detail',
  templateUrl: './consumabile-detail.component.html',
  styleUrls: ['./consumabile-detail.component.css']
})
export class ConsumabileDetailComponent implements OnInit {
  
  inputId: number;
  inputCodice: string = "";
  inputNome: string = "";
  inputDescrizione: string = "";
  inputTipologia: string = "";

  showTuttoOk : Boolean = false;
  showDettaglioCons : Boolean = true;

  constructor(
    private rottaAttiva: ActivatedRoute,
    private rest : RestService,
    private router : Router
    ) { 

    // let persona = {
    //   nome: "Giovanni",
    //   cognome: "Pace"
    // }
    // console.log(persona);

  }

  ngOnInit(): void {

    this.rottaAttiva.params.subscribe(
      (parametri) => {
        let codiceSelezionato = parametri.codice;
        //TODO: Prendi i dettagli del consumabile

        this.rest.getDettaglioOggetto(codiceSelezionato).subscribe(
          (risultato) => {
            if(risultato == null){
              this.showDettaglioCons = false;
              return;
            }

            this.inputId = risultato.id;
            this.inputNome = risultato.nome;
            this.inputDescrizione = risultato.descrizione;
            this.inputCodice = risultato.codice;
            this.inputTipologia = risultato.tipologia;
          },         //SUCCESS
          (errore) => {
            console.log(errore);
          }          //ERROR
        );

      }
    );

  }

  eliminaConsumabile(){

    this.rest.deleteOggetto(this.inputCodice).subscribe(
      (risultato) => {
        console.log(risultato);
        if(risultato == true){
          this.showTuttoOk = true;
          
          setTimeout(() => {
            this.router.navigateByUrl('consumabile/list');
          }, 2000);
        }
      },       //SUCCESS
      (errore) => {
        console.log(errore);
      }  
    );

  }

  modificaConsumabile(){
    
    let temp = new Consumabile();
    temp.codice = this.inputCodice;
    temp.nome = this.inputNome;
    temp.descrizione = this.inputDescrizione;
    temp.tipologia = this.inputTipologia;

    this.rest.updateOggetto(temp).subscribe(
      (risultato) => {

        if(
          risultato.nome == temp.nome &&
          risultato.codice == temp.codice &&
          risultato.descrizione == temp.descrizione &&
          risultato.tipologia == temp.tipologia
        ){
          this.showTuttoOk = true;

          setTimeout(() => {
            this.router.navigateByUrl('consumabile/list');
          }, 2000);
        }

      },
      (errore) => {
        console.log("Errore");
      },
    );
  }

}
