import { Component, OnInit } from '@angular/core';
import { Persona } from '../persona';

@Component({
  selector: 'app-prove-interne',
  templateUrl: './prove-interne.component.html',
  styleUrls: ['./prove-interne.component.css']
})
export class ProveInterneComponent implements OnInit {

  dimmiCiao(varValore){
    console.log(varValore);
  }

  funzioneSaluta(){
    console.log("CIAO");
  }

  sommaNumeri(varPrimo :number, varSecondo :number) :number{
    return varPrimo + varSecondo;
  }

  stampaVariabile(){
    console.log(this.valoreInput);
  }

  numeroUno :number = 34; 
  elenco :any = [];
  // persona :any = {};
  studente :Persona;
  professore :Persona = new Persona();

  valoreInput : string = "Giovanni";   

  constructor() { 
    // this.dimmiCiao("CIAO dal Costruttore");

    // let somma = this.sommaNumeri(6, 5);
    // console.log(somma);

    // console.log(this.numeroUno);

    // this.elenco.push("Giovanni");
    // this.elenco.push("Marco");
    // this.elenco.push("Maria");
    // console.log(this.elenco);

    // this.persona.nome = "Giovanni";
    // this.persona.cognome = "Pace";
    // console.log(this.persona);

    this.studente = new Persona();
    this.studente.cognome = "Pace";
    this.studente.nome = "Giovanni";
    console.log(this.studente);

    this.professore.nome = "Ciaaio";
    this.professore.cognome = "gijoegorejigres";
    this.professore.eta = 34;
    console.log(this.professore);

  }

  ngOnInit(): void {
    // this.dimmiCiao("Ciao dal onInit");
  }
}
