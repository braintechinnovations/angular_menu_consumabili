import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProveInterneComponent } from './prove-interne.component';

describe('ProveInterneComponent', () => {
  let component: ProveInterneComponent;
  let fixture: ComponentFixture<ProveInterneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProveInterneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProveInterneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
